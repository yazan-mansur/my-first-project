<!-- Edit 1 --> <!-- Edit 2 --><!-- Return Edit 3 -->
<!--------------- Connect With Database ------------------>
<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "courses";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);

  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Task 1 => Yazan Mansur</title>
        <link rel="stylesheet" href="css/bootstrap.css"/>
        <link rel="stylesheet" href="css/taskStyle.css"/>
    </head>
    <body>
        <!--------------------------------- Start Navbar Header ---------------------------------------->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../task1/">Udemy</a>
              </div>
          
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="../task1/">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--------------------------------- End Navbar Header ---------------------------------------->

        <!--------------------------------- Start Delete Image Page ---------------------------------------->
        <?php 
          $userName = $_GET["userName"];
          $img_dir = 'uploads/' . $userName . '/';
          $image_name = $_GET['image']; // Get The Image Name From The Link
          
          // unlink function job => delete the image from it path => unlink(imagePath) and return true if this image really exists inside this path and delete process complete successfully 
          //unlink function return bool so you can use it as conditon
          if(unlink($img_dir . $image_name)){
              $sql = "UPDATE users SET image=null WHERE image='" . $image_name . "'";
              if ($conn->query($sql) === TRUE) {
                  echo '<div class="alert alert-success upload-suc-dan">';
                      echo "Your Image Has Been Deleted successfully";
                  echo '</div>';
                } else {
                  echo '<div class="alert alert-danger upload-suc-dan">';
                      echo "Error Deleting Image: " . $conn->error;
                  echo '</div>';
                }
          }else{
              echo '<div class="alert alert-danger upload-suc-dan">';
                  echo 'ERROR: unable to delete image file!';
              echo '</div>';
          }
          // Redirect To The Profile Page After 3 Seconds Waiting
          header('refresh: 3; url=http://localhost/task1/profile.php?userID=' . $_GET['userID']);
          $conn->close();
        ?>
        <!--------------------------------- End Delete Image Page ---------------------------------------->

        <!--------------------------------- Start Navbar Footer ---------------------------------------->
        <nav class="navbar navbar-inverse footer">
            <div class="container-fluid">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                  <li><a>Phone: +963 011 0000000</a></li>
                  <li><a>Address: Damascus, Syria</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--------------------------------- End Navbar Footer ---------------------------------------->

        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/taskScript.js"></script>
    </body>
</html>